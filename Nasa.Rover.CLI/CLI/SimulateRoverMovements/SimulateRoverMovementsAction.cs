﻿using CLI.SimulateRoverMovements;
using Nasa.Rover.Application.Commands.SimulateRoverMovementPlan;
using Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Command;
using Nasa.Rover.Domain.Rovers.Builder;
using Nasa.Rover.Domain.Rovers.Service.ApplyMovementPlan;
using Nasa.Rover.Domain.Rovers.Service.Move;
using Nasa.Rover.Domain.Rovers.Service.Redirect;

namespace CLI.Actions.SimulateRoverMovements
{
    public class SimulateRoverMovementsAction
    {
        public static async Task Execute()
        {
            //Dependencies
            //TODO: USE DI RESOLVER
            var moveRover = new MoveRover();
            var redirectRover = new RedirectRover();
            var roverMovementPlanApplier = new ApplyMovementPlan(moveRover, redirectRover);
            var validator = new SimulateRoverMovementPlanCommandValidator();
            var builder = new RoverBuilder();
            var handler = new SimulateRoverMovementPlanCommandHandler(
                validator,
                builder,
                roverMovementPlanApplier);


            var roverCount = 1;

            Console.Write("Enter Graph Upper Right Coordinate: ");
            var maxCoordinateData = ReadMaxCoordinateData.Execute(Console.ReadLine());

            while (true)
            {
                Console.Write($"Rover {roverCount} Starting Position: ");
                var roverData = ReadRoverData.Execute(Console.ReadLine());

                Console.Write($"Rover {roverCount} Movement Plan: ");
                var movementPlanData = ReadMovementPlanData.Execute(Console.ReadLine());

                //Apply the movement plan
                var command = new SimulateRoverMovementPlanCommand
                {
                    MaxCoordinateX = maxCoordinateData.X,
                    MaxCoordinateY = maxCoordinateData.Y,
                    RoverCoordinateX = roverData.X,
                    RoverCoordinateY = roverData.Y,
                    RoverDirection = roverData.Direction,
                    MovementPlan = movementPlanData
                };
                var dto = await handler.Handle(command, CancellationToken.None);
                Console.WriteLine($"Rover {roverCount} Output: {dto.RoverCoordinateX} {dto.RoverCoordinateY} {dto.RoverDirection}");

                roverCount++;
            }
        }
    }
}
