﻿namespace CLI.SimulateRoverMovements
{
    public static class ReadMovementPlanData
    {
        static string ErrorInvalidInput => "The Movement Plan must be contain at least one movement.";

        public static IEnumerable<char> Execute(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            var movements = input
                .Trim()
                .ToCharArray();

            if (movements.Length == 0)
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            return movements;
        }
    }
}
