﻿namespace CLI.Actions.SimulateRoverMovements
{
    public static class ReadRoverData
    {
        static string ErrorInvalidInput => "To initialize a new Rover specify CoordinateX, CoordinateY and a Cardinal Point values separated by only one space char.";

        public static RoverData Execute(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            var roverParameters = input
                .Trim()
                .Split(' ');

            if (roverParameters.Length != 3)
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            int x;
            int y;

            var validParsedData = int.TryParse(roverParameters[0], out x) & int.TryParse(roverParameters[1], out y);

            if (!validParsedData)
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            return new()
            {
                X = x,
                Y = y,
                Direction = char.Parse(roverParameters[2]),
            };
        }
    }

    public class RoverData
    {
        public int X { get; set; }

        public int Y { get; set; }

        public char Direction { get; set; }
    }
}
