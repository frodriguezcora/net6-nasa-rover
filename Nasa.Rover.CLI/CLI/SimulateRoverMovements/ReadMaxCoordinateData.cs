﻿namespace CLI.SimulateRoverMovements
{
    public static class ReadMaxCoordinateData
    {
        static string ErrorInvalidInput => "To define the max coordinate specify CoordinateX and CoordinateY values separated by only one space char.";

        public static MaxCoordinateData Execute(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            var coordinatePoints = input
                .Trim()
                .Split(' ');

            if (coordinatePoints.Length != 2)
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            int x;
            int y;

            var validParsedData = int.TryParse(coordinatePoints[0], out x) & int.TryParse(coordinatePoints[1], out y);

            if (!validParsedData)
            {
                throw new ArgumentException(ErrorInvalidInput);
            }

            return new()
            {
                X = x,
                Y = y,
            };
        }
    }

    public class MaxCoordinateData
    {
        public int X { get; set; }

        public int Y { get; set; }
    }
}
