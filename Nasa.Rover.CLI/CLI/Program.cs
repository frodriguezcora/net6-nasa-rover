﻿using CLI.Actions.SimulateRoverMovements;


static async void RunCli()
{
    try
    {
        await SimulateRoverMovementsAction.Execute();
    }
    catch (Exception error)
    {
        Console.WriteLine();
        Console.WriteLine();
        Console.WriteLine("------------------------START ERROR-------------------------");
        Console.WriteLine();
        Console.WriteLine(error.Message);
        Console.WriteLine();
        Console.WriteLine("------------------------FINISH ERROR-------------------------");
        Console.WriteLine();
    }
}

RunCli();