using MediatR;
using Microsoft.AspNetCore.Mvc;
using Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Command;
using Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Dto;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Rover : ControllerBase
    {
        private readonly ISender sender;

        public Rover(ISender sender)
        {
            this.sender = sender;
        }

        [HttpPut]
        [Route("simulate-movements")]
        public async Task<SimulateRoverMovementPlanCommandDto> Post(SimulateRoverMovementPlanCommand command)
        {
            var dto = await sender.Send(command);
            return dto;
        }
    }
}