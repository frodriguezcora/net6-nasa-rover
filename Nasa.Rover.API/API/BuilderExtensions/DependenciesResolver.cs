﻿using FluentValidation;
using MediatR;
using System.Reflection;

namespace Permissions.API.BuilderExtensions
{
    public static class DependenciesResolver
    {
        private const string MediatorDependencies = "Nasa.Rover.Application";
        private const string ValidatorDependencies = "Nasa.Rover.Application";

        private static IEnumerable<string> ScopedDependencies = new[]
        {
            "Nasa.Rover.Application",
        };

        private static IEnumerable<string> TransientDependencies = new[]
        {
            "Nasa.Rover.Domain",
        };

        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.Load(MediatorDependencies));
            services.AddValidatorsFromAssembly(Assembly.Load(ValidatorDependencies));

            ResolveScopedDependencies(services);
            ResolveTransientDependencies(services);

            return services;
        }

        private static void ResolveScopedDependencies(IServiceCollection services)
        {
            var asemblies = ScopedDependencies.Select(namespaceString => Assembly.Load(namespaceString));

            services.Scan(scan =>
                scan.FromAssemblies(asemblies)
                    .AddClasses()
                    .AsMatchingInterface()
                    .WithScopedLifetime());
        }

        private static void ResolveTransientDependencies(IServiceCollection services)
        {
            var asemblies = TransientDependencies.Select(namespaceString => Assembly.Load(namespaceString));

            services.Scan(scan =>
                scan.FromAssemblies(asemblies)
                    .AddClasses()
                    .AsMatchingInterface()
                    .WithScopedLifetime());
        }

    }
}
