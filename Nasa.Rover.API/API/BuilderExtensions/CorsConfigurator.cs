﻿namespace Permissions.API.BuilderExtensions
{
    public static class CorsConfigurator
    {
        public static WebApplication EnableCors(this WebApplication app)
        {

            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .SetIsOriginAllowed((host) => true)
                .AllowCredentials()
            );

            return app;
        }
    }
}
