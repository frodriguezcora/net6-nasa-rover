# NET6-NASA-ROVER



## SPECIFICATION

A squad of robotic rovers are to be landed by NASA on a plateau on Mars. The navigation team needs a utility for them to simulate rover movements so they can develop a navigation plan.

A rover's position is represented by a combination of an x and y co-ordinates and a letter representing one of the four cardinal compass points. The plateau is divided up nto a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing North.

In order to control a rover, NASA sends a simple string of letters. The possible letters are:
- Make the rover spin 90 degrees left without moving from its current spot
- Make the rover spin 90 degrees right without moving from its current spot
- Move forward one grid point, and maintain the same heading.
Assume that the square directly North from (x, y) is (x, y+1).
