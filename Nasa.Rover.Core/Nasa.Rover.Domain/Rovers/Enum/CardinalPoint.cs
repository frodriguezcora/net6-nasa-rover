﻿namespace Nasa.Rover.Domain.Rovers.Enum
{
    public enum CardinalPoint
    {
        North = 1,
        South,
        West,
        East,
    }
}
