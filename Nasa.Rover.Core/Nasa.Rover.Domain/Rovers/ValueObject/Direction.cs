﻿using Nasa.Rover.Domain.Rovers.Enum;

namespace Nasa.Rover.Domain.Rovers.ValueObject
{
    public class Direction
    {
        private const string ErrorRoverDirection = "Invalid Rover Direction";

        private const char North = 'N';
        private const char South = 'S';
        private const char West = 'W';
        private const char East = 'E';

        public Direction(char directionValue)
        {
            Value = directionValue switch
            {
                North => CardinalPoint.North,
                South => CardinalPoint.South,
                West => CardinalPoint.West,
                East => CardinalPoint.East,
                _ => throw new DomainException(ErrorRoverDirection)
            };
        }

        public Direction(CardinalPoint cardinalPoint)
        {
            Value = cardinalPoint;
        }

        public CardinalPoint Value { get; }

        public char Text
        {
            get => Value switch
            {
                CardinalPoint.North => North,
                CardinalPoint.South => South,
                CardinalPoint.West => West,
                CardinalPoint.East => East,
                _ => throw new DomainException(ErrorRoverDirection)
            };
        }
    }
}
