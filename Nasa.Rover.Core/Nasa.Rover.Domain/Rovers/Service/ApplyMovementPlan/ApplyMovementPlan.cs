﻿using Nasa.Rover.Domain.MovementPlans;
using Nasa.Rover.Domain.MovementPlans.Enum;
using Nasa.Rover.Domain.Rovers.Service.Move;
using Nasa.Rover.Domain.Rovers.Service.Redirect;
using Nasa.Rover.Domain.Rovers.ValueObject;

namespace Nasa.Rover.Domain.Rovers.Service.ApplyMovementPlan
{
    public class ApplyMovementPlan : IApplyMovementPlan
    {
        private readonly IMoveRover moveRover;
        private readonly IRedirectRover redrectRover;

        public ApplyMovementPlan(
            IMoveRover moveRover,
            IRedirectRover redrectRover)
        {
            this.moveRover = moveRover;
            this.redrectRover = redrectRover;
        }

        public void Execute(Rover rover, MovementPlan movementPlan, Coordinate maxCoordinate)
        {
            foreach (var movement in movementPlan.Movements)
            {
                if (movement.Value == Instruction.Move)
                {
                    moveRover.Execute(rover, maxCoordinate);
                }

                redrectRover.Execute(rover, movement);
            }
        }
    }
}
