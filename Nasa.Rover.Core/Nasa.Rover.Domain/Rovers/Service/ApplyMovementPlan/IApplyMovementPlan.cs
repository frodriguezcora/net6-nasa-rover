﻿using Nasa.Rover.Domain.MovementPlans;
using Nasa.Rover.Domain.Rovers.ValueObject;

namespace Nasa.Rover.Domain.Rovers.Service.ApplyMovementPlan
{
    public interface IApplyMovementPlan
    {
        void Execute(Rover rover, MovementPlan movementPlan, Coordinate maxCoordinate);
    }
}
