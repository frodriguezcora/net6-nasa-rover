﻿using Nasa.Rover.Domain.MovementPlans.ValueObject;

namespace Nasa.Rover.Domain.Rovers.Service.Redirect
{
    public interface IRedirectRover
    {
        void Execute(Rover rover, Movement movement);
    }
}
