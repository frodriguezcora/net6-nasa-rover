﻿using Nasa.Rover.Domain.MovementPlans.Enum;
using Nasa.Rover.Domain.MovementPlans.ValueObject;
using Nasa.Rover.Domain.Rovers.Enum;

namespace Nasa.Rover.Domain.Rovers.Service.Redirect
{
    public class RedirectRover : IRedirectRover
    {
        public void Execute(Rover rover, Movement movement)
        {
            if (movement.Value == Instruction.Move)
            {
                return;
            }

            switch (rover.Direction.Value)
            {
                case CardinalPoint.North:
                    switch (movement.Value)
                    {
                        case Instruction.Left:
                            rover.Direction = new(CardinalPoint.West);
                            return;
                        default:
                            rover.Direction = new(CardinalPoint.East);
                            return;
                    }
                case CardinalPoint.South:
                    switch (movement.Value)
                    {
                        case Instruction.Left:
                            rover.Direction = new(CardinalPoint.East);
                            return;
                        default:
                            rover.Direction = new(CardinalPoint.West);
                            return;
                    }
                case CardinalPoint.East:
                    switch (movement.Value)
                    {
                        case Instruction.Left:
                            rover.Direction = new(CardinalPoint.North);
                            return;
                        default:
                            rover.Direction = new(CardinalPoint.South);
                            return;
                    }
                default:
                    switch (movement.Value)
                    {
                        case Instruction.Left:
                            rover.Direction = new(CardinalPoint.South);
                            return;
                        default:
                            rover.Direction = new(CardinalPoint.North);
                            return;
                    }
            }
        }
    }
}
