﻿using Nasa.Rover.Domain.Rovers.Enum;
using Nasa.Rover.Domain.Rovers.ValueObject;

namespace Nasa.Rover.Domain.Rovers.Service.Move
{
    public class MoveRover : IMoveRover
    {
        public void Execute(Rover rover, Coordinate maxCoordinate)
        {

            switch (rover.Direction.Value)
            {
                case CardinalPoint.North:
                    if (rover.Coordinate.Y + 1 <= maxCoordinate.Y)
                    {
                        rover.Coordinate.Y++;
                    }
                    return;
                case CardinalPoint.South:
                    if (rover.Coordinate.Y - 1 >= 0)
                    {
                        rover.Coordinate.Y--;
                    }
                    return;
                case CardinalPoint.East:
                    if (rover.Coordinate.X + 1 <= maxCoordinate.X)
                    {
                        rover.Coordinate.X++;
                    }
                    return;
                default:
                    if (rover.Coordinate.X - 1 >= 0)
                    {
                        rover.Coordinate.X--;
                    }
                    return;
            }
        }
    }
}
