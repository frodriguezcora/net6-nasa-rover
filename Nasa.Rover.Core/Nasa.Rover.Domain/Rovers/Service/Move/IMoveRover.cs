﻿using Nasa.Rover.Domain.Rovers.ValueObject;

namespace Nasa.Rover.Domain.Rovers.Service.Move
{
    public interface IMoveRover
    {
        void Execute(Rover rover, Coordinate maxCoordinate);
    }
}
