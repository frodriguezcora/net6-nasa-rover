﻿using Nasa.Rover.Domain.Rovers.ValueObject;

namespace Nasa.Rover.Domain.Rovers.Builder
{
    public class RoverBuilder : IRoverBuilder
    {
        private const string ErrorInvalidRover = "Invalid Rover Building";

        private int? CoordinateX;
        private int? CoordinateY;
        private Direction? Direction;

        public IRoverBuilder SetCoordinateX(int x)
        {
            CoordinateX = x;
            return this;
        }

        public IRoverBuilder SetCoordinateY(int y)
        {
            CoordinateY = y;
            return this;
        }

        public IRoverBuilder SetDirection(char direction)
        {
            Direction = new Direction(char.ToUpper(direction));
            return this;
        }

        public Rover Build()
        {
            if (CoordinateX is null || CoordinateY is null || Direction is null)
            {
                throw new DomainException(ErrorInvalidRover);
            }

            var instance = new Rover
            {
                Coordinate = new()
                {
                    X = CoordinateX.Value,
                    Y = CoordinateY.Value
                },
                Direction = Direction,
            };

            Reset();

            return instance;
        }

        private void Reset()
        {
            CoordinateX = null;
            CoordinateY = null;
            Direction = null;
        }
    }
}
