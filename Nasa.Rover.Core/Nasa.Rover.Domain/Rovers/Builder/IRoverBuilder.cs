﻿namespace Nasa.Rover.Domain.Rovers.Builder
{
    public interface IRoverBuilder
    {
        IRoverBuilder SetCoordinateX(int x);

        IRoverBuilder SetCoordinateY(int y);

        IRoverBuilder SetDirection(char direction);

        Rover Build();
    }
}
