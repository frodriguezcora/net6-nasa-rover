﻿using Nasa.Rover.Domain.Rovers.ValueObject;

namespace Nasa.Rover.Domain.Rovers
{
    public class Rover
    {
        public Coordinate Coordinate { get; set; }

        public Direction Direction { get; set; }
    }
}
