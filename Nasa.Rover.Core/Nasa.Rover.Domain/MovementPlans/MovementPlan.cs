﻿using Nasa.Rover.Domain.MovementPlans.ValueObject;

namespace Nasa.Rover.Domain.MovementPlans
{
    public class MovementPlan
    {
        public MovementPlan(IEnumerable<Movement> movements)
        {
            Movements = movements;
        }

        public IEnumerable<Movement> Movements { get; }
    }
}
