﻿using Nasa.Rover.Domain.MovementPlans.Enum;

namespace Nasa.Rover.Domain.MovementPlans.ValueObject
{
    public class Movement
    {
        private const string ErrorMovementDirection = "Invalid Movement";

        private const char Left = 'L';
        private const char Right = 'R';
        private const char Move = 'M';

        public Movement(char movement)
        {
            Value = char.ToUpper(movement) switch
            {
                Left => Instruction.Left,
                Right => Instruction.Right,
                Move => Instruction.Move,
                _ => throw new DomainException(ErrorMovementDirection)
            };
        }

        public Instruction Value { get; }
    }
}
