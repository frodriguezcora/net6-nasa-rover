﻿namespace Nasa.Rover.Domain.MovementPlans.Enum
{
    public enum Instruction
    {
        Left = 1,
        Right,
        Move
    }
}
