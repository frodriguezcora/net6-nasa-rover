﻿using FluentValidation;
using MediatR;
using Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Command;
using Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Dto;
using Nasa.Rover.Domain.MovementPlans;
using Nasa.Rover.Domain.MovementPlans.ValueObject;
using Nasa.Rover.Domain.Rovers.Builder;
using Nasa.Rover.Domain.Rovers.Service.ApplyMovementPlan;
using Nasa.Rover.Domain.Rovers.ValueObject;

namespace Nasa.Rover.Application.Commands.SimulateRoverMovementPlan
{
    public class SimulateRoverMovementPlanCommandHandler : IRequestHandler<SimulateRoverMovementPlanCommand, SimulateRoverMovementPlanCommandDto>
    {
        private readonly IValidator<SimulateRoverMovementPlanCommand> validator;
        private readonly IRoverBuilder roverBuilder;
        private readonly IApplyMovementPlan applyMovementPlan;

        public SimulateRoverMovementPlanCommandHandler(
            IValidator<SimulateRoverMovementPlanCommand> validator,
            IRoverBuilder roverBuilder,
            IApplyMovementPlan applyMovementPlan)
        {
            this.validator = validator;
            this.roverBuilder = roverBuilder;
            this.applyMovementPlan = applyMovementPlan;
        }

        public Task<SimulateRoverMovementPlanCommandDto> Handle(SimulateRoverMovementPlanCommand command, CancellationToken cancellationToken)
        {
            validator.ValidateAndThrow(command);

            var maxCoordinate = new Coordinate
            {
                X = command.MaxCoordinateX,
                Y = command.MaxCoordinateY,
            };

            var movements = command.MovementPlan.Select(x => new Movement(x));
            var movementPlan = new MovementPlan(movements);

            var roverInstance = roverBuilder
            .SetCoordinateX(command.RoverCoordinateX)
            .SetCoordinateY(command.RoverCoordinateY)
            .SetDirection(command.RoverDirection)
            .Build();

            applyMovementPlan.Execute(
                roverInstance,
                movementPlan,
                maxCoordinate);

            var dto = new SimulateRoverMovementPlanCommandDto
            {
                RoverCoordinateX = roverInstance.Coordinate.X,
                RoverCoordinateY = roverInstance.Coordinate.Y,
                RoverDirection = roverInstance.Direction.Text,
            };

            return Task.FromResult(dto);
        }
    }
}