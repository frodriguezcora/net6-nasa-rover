﻿using FluentValidation;

namespace Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Command
{
    public class SimulateRoverMovementPlanCommandValidator : AbstractValidator<SimulateRoverMovementPlanCommand>
    {
        public SimulateRoverMovementPlanCommandValidator()
        {
            RuleFor(x => x.MaxCoordinateX).NotNull();
            RuleFor(x => x.MaxCoordinateY).NotNull();
            RuleFor(x => x.RoverCoordinateX).NotNull();
            RuleFor(x => x.RoverCoordinateY).NotNull();
            RuleFor(x => x.RoverCoordinateY).NotNull();
            RuleFor(x => x.MovementPlan).NotEmpty();
        }
    }
}
