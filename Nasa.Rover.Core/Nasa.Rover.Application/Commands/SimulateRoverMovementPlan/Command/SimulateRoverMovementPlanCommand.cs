﻿using MediatR;
using Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Dto;

namespace Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Command
{
    public class SimulateRoverMovementPlanCommand : IRequest<SimulateRoverMovementPlanCommandDto>
    {
        public int MaxCoordinateX { get; set; }

        public int MaxCoordinateY { get; set; }

        public int RoverCoordinateX { get; set; }

        public int RoverCoordinateY { get; set; }

        public char RoverDirection { get; set; }

        public IEnumerable<char> MovementPlan { get; set; }
    }
}