﻿namespace Nasa.Rover.Application.Commands.SimulateRoverMovementPlan.Dto
{
    public class SimulateRoverMovementPlanCommandDto
    {
        public int RoverCoordinateX { get; set; }

        public int RoverCoordinateY { get; set; }

        public char RoverDirection { get; set; }
    }
}